package uk.ac.manchester.cs.chainsaw;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;

import utils.cachedreasoner.CachedOWLReasoner;

public class ChainsawReasonerFactory implements OWLReasonerFactory {
	private final OWLReasonerFactory factory;

	public ChainsawReasonerFactory() {
		this("uk.ac.manchester.cs.factplusplus.owlapiv3.FaCTPlusPlusReasonerFactory");
	}

	public ChainsawReasonerFactory(String factoryName) {
		try {
			factory = (OWLReasonerFactory) Class.forName(factoryName).newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Cannot find " + factoryName + " on the classpath", e);
		}
	}

	public String getReasonerName() {
		return "Chainsaw";
	}

	public OWLReasoner createReasoner(OWLOntology ontology) {
		ChainsawReasoner toReturn = new ChainsawReasoner(factory, ontology, new SimpleConfiguration());
		return verify(toReturn);
	}

	private OWLReasoner verify(ChainsawReasoner toReturn) {
		OWLOntologyManager m = toReturn.getRootOntology().getOWLOntologyManager();
		final CachedOWLReasoner cachedOWLReasoner = new CachedOWLReasoner(toReturn, m);
		m.addOntologyChangeListener(toReturn);
		m.addOntologyChangeListener(cachedOWLReasoner);
		return cachedOWLReasoner;
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology ontology) {
		ChainsawReasoner toReturn = new ChainsawReasoner(factory, ontology, new SimpleConfiguration());
		return verify(toReturn);
	}

	public OWLReasoner createReasoner(OWLOntology ontology, OWLReasonerConfiguration config)
			throws IllegalConfigurationException {
		ChainsawReasoner toReturn = new ChainsawReasoner(factory, ontology, config);
		return verify(toReturn);
	}

	public OWLReasoner createNonBufferingReasoner(OWLOntology ontology, OWLReasonerConfiguration config)
			throws IllegalConfigurationException {
		ChainsawReasoner toReturn = new ChainsawReasoner(factory, ontology, config);
		return verify(toReturn);
	}
}
