package uk.ac.manchester.cs.chainsaw;

import java.util.Set;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;
import org.semanticweb.owlapi.reasoner.knowledgeexploration.OWLKnowledgeExplorerReasoner;

import uk.ac.manchester.cs.atomicdecomposition.*;
import uk.ac.manchester.cs.jfact.*;
import uk.ac.manchester.cs.jfact.split.ModuleType;

public class JFactChainsawReasoner extends ChainsawReasoner {
	private JFactReasoner myReasoner;

	public JFactChainsawReasoner(final OWLReasonerFactory f, final OWLOntology o, final OWLReasonerConfiguration config) {
		super(f, o, config);
	}

	@Override
	protected void initModularisationReasoner(final OWLOntology o) {
		if (myReasoner != null) {
			myReasoner.dispose();
		}
		myReasoner = (JFactReasoner) new JFactFactory().createReasoner(o);
	}

	@Override
	protected AtomicDecomposition getAtomicDecomposer() {
		return new AtomicDecomposerOWLAPITOOLS(rootOntology);
	}

	@Override
	protected Set<OWLAxiom> moduleStrategy(final Set<OWLEntity> sig, final boolean b, final int moduleTechnique) {
		return myReasoner.getModule(sig, false, ModuleType.values()[moduleTechnique]);
	}

	@Override
	protected OWLKnowledgeExplorerReasoner getExplorationDelegate(Set<OWLAxiom> axioms) {
		JFactReasoner delegate = (JFactReasoner) getDelegate(axioms, true);
		delegate.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		return delegate;
	}

}
