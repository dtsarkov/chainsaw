package uk.ac.manchester.cs.chainsaw;

import java.util.*;

import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;
import org.semanticweb.owlapi.reasoner.impl.*;
import org.semanticweb.owlapi.util.MultiMap;

import uk.ac.manchester.cs.atomicdecomposition.*;

public class ChainsawClassifier {
	private final MultiMap<OWLClass, OWLClass> subClasses;
	private final MultiMap<OWLClass, OWLClass> superClasses;
	private final Set<Atom> classifiedAtoms;
	private final Map<OWLClass, OWLClass> synonymOf;
	private final MultiMap<OWLClass, OWLClass> synonyms;

	private AtomicDecomposition ad;

	private ChainsawReasoner Chainsaw;
	final private Map<Atom, OWLReasoner> ReasonerCache = new HashMap<Atom, OWLReasoner>();
	private OWLReasoner delegate;

	private OWLClass getRepresentative(OWLClass cls) {
		if (synonymOf.containsKey(cls)) {
			return synonymOf.get(cls);
		}
		return cls;
	}

	public ChainsawClassifier(ChainsawReasoner chainsaw, AtomicDecomposition AD) {
		Chainsaw = chainsaw;
		ad = AD;
		superClasses = new MultiMap<OWLClass, OWLClass>();
		subClasses = new MultiMap<OWLClass, OWLClass>();
		synonymOf = new HashMap<OWLClass, OWLClass>();
		synonyms = new MultiMap<OWLClass, OWLClass>();
		classifiedAtoms = new HashSet<Atom>();

		// init synonyms for constants
		OWLDataFactory df = Chainsaw.rootOntology.getOWLOntologyManager().getOWLDataFactory();
		synonyms.put(df.getOWLThing(), df.getOWLThing());
		synonyms.put(df.getOWLNothing(), df.getOWLNothing());

		// init reasoner map
		// for (Atom topAtom : ad.getTopAtoms()) {
		// // mode B
		// System.out.println("ChainsawClassifier.ChainsawClassifier(): delegate "
		// + (i++));
		// ReasonerCache.put(topAtom,
		// Chainsaw.getDelegate(ad.getPrincipalIdeal(topAtom), true));
		// }

		// build superclasses
		for (Atom atom : ad.getTopAtoms()) {
			delegate = Chainsaw.getDelegate(ad.getPrincipalIdeal(atom), true);
			buildTaxonomyBit(atom);
		}

		// build subclasses
		for (OWLClass cls : superClasses.keySet()) {
			for (OWLClass sup : superClasses.get(cls)) {
				subClasses.put(sup, cls);
			}
		}
	}

	private OWLReasoner getReasoner(Atom atom) {
		return delegate;
		// for (Atom d : ad.getDependents(atom)) {
		// if (ad.isTopAtom(d)) {
		// return ReasonerCache.get(d);
		// }
		// }
		// // impossible
		// return null;
	}

	private void buildTaxonomyBit(Atom atom) {
		for (Atom dep : ad.getDependencies(atom, true))
			buildTaxonomyBit(dep);
		classifyAtom(atom);
		classifiedAtoms.add(atom);
	}

	private void classifyAtom(Atom atom) {
		// check timeout
		if (System.currentTimeMillis() - Chainsaw.startTime > Chainsaw.getTimeOut()) {
			throw new TimeOutException();
		}
		// for every thing in sig
		OWLReasoner subChecker = getReasoner(atom);// Chainsaw.getDelegate(ad.getPrincipalIdeal(atom));
		for (OWLEntity e : atom.getLabel())
			if (e.isOWLClass()) {
				OWLClass cls = (OWLClass) e;
				// add cls as its own synonym
				synonyms.put(cls, cls);

				if (!subChecker.isSatisfiable(cls)) {
					// synonymOf.put(key, value)
				}
				Set<OWLClass> supers = subChecker.getSuperClasses(cls, true).getFlattened();
				// build all synonyms
				Set<OWLClass> eqs = subChecker.getEquivalentClasses(cls).getEntities();
				for (OWLClass syn : eqs) {
					if (!syn.equals(cls)) {
						synonymOf.put(syn, cls);
						synonyms.put(cls, syn);
					}
				}

				// replace all synonyms with reps
				for (OWLClass syn : synonymOf.keySet()) {
					if (supers.contains(syn)) {
						supers.remove(syn);
						supers.add(synonymOf.get(syn));
					}
				}
				supers.remove(cls);
				// make the known part of a taxonomy
				superClasses.putAll((OWLClass) e, supers);
			}
	}

	private Node<OWLClass> getNode(OWLClass cls) {
		return new OWLClassNode((Set<OWLClass>) synonyms.get(getRepresentative(cls)));
	}

	private Set<Node<OWLClass>> getHierarchyPart(Set<OWLClass> visited, OWLClass start,
			MultiMap<OWLClass, OWLClass> neighbours, boolean direct) {
		Set<Node<OWLClass>> toReturn = new HashSet<Node<OWLClass>>();
		// check the visited ones
		if (visited.contains(start))
			return toReturn;

		// cur one is checked
		visited.add(start);

		// current node is in there
		toReturn.add(getNode(start));

		// if direct -- just add the one and we're done
		if (direct)
			return toReturn;

		for (OWLClass neigh : neighbours.get(start)) {
			toReturn.addAll(getHierarchyPart(visited, neigh, neighbours, false));
		}

		return toReturn;
	}

	private NodeSet<OWLClass> getHierarchy(OWLClass start, MultiMap<OWLClass, OWLClass> neighbours, boolean direct) {
		OWLClassNodeSet toReturn = new OWLClassNodeSet();
		Set<OWLClass> visited = new HashSet<OWLClass>();
		for (OWLClass neigh : neighbours.get(start)) {
			toReturn.addAllNodes(getHierarchyPart(visited, neigh, neighbours, direct));
		}
		return toReturn;
	}

	public NodeSet<OWLClass> getSubClasses(OWLClass start, boolean direct) {
		return getHierarchy(start, subClasses, direct);
	}

	public NodeSet<OWLClass> getSuperClasses(OWLClass start, boolean direct) {
		return getHierarchy(start, superClasses, direct);
	}

	public Node<OWLClass> getEquivalentClasses(OWLClass start) {
		return getNode(start);
	}

}